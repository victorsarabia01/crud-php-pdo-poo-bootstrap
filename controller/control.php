<?php 

include_once 'model/persona.php';
	//CONTROLADOR MAS FUNCION QUE INVOCA LA VISTA 
	class control{
		public $mode;

		public function __construct(){
			$this->mode = new persona();
		}

		public function index(){
			include_once 'view/home.php'; //INVOCAR A LA VISTA
		}
		
		public function nuevo(){
			$alm = new persona();
			if(isset($_REQUEST['id'])){
				$alm = $this->mode->cargarID($_REQUEST['id']);
			}
			include_once 'view/save.php';
		}
		public function guardar(){
			$alm = new persona();
			$alm->id = $_POST['txtID'];
			$alm->nombre = $_POST['txtNombre'];
			$alm->apellido = $_POST['txtApellido'];
			$alm->edad = $_POST['txtEdad'];
			$alm->fecha = $_POST['fecha'];
			$alm->id_genero = $_POST['genero'];
			

			$alm->id > 0 ? $this->mode->actualizarDatos($alm) : $this->mode->registrar($alm);
			
			header("Location: index.php");
		}
		public function eliminar(){
			$this->mode->delete($_REQUEST['id']);
			header("Location: index.php");
		}
	}
 ?>