<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="resources/css/bootstrap.css">
	<script type="text/javascript" src="resources/js/bootstrap.min.js"></script>
</head>
<body>


<div class="container">
    <div class="row align-items-center">
        <div class="col-md-12">
            <div class="well well-sm">
                <form class="form-horizontal" method="post" action="?c=guardar">
                    <fieldset>
                        <legend class="header">Registro de Personas</legend>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                            <div class="col-md-8">
                            	<input type="hidden" name="txtID" value="<?php echo $alm->id; ?>">
                                <input  name="txtNombre" type="text" value="<?php echo $alm->nombre; ?>" placeholder="nombre" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>
                            <div class="col-md-8">
                                <input  name="txtApellido" type="text" value="<?php echo $alm->apellido; ?>" placeholder="Apellido" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope-o bigicon"></i></span>
                            <div class="col-md-8">
                                <input  name="txtEdad" type="text" value="<?php echo $alm->edad; ?>" placeholder="Edad" class="form-control">
                            </div>
                        </div>

                        <div class="form-group">
                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>
                            <div class="col-md-8">
                                <input  name="fecha" type="date" value="<?php echo $alm->fecha; ?>" placeholder="fecha" class="form-control">
                            </div>
                        </div>

				        <div class="form-group">
							<div class="col-md-3">
								<select name="genero">
									<?php foreach ($this->mode->cargarGenero()  as $k) : ?>
										<option value="<?php echo $k->id_genero ?>" <?php echo $k->id_genero == $alm->id_genero? 'selected' : ''; ?>><?php echo $k->nombre_genero ?></option>
									<?php endforeach ?>
								</select>
							</div>
						</div>
                        <br>
                        <div class="form-group">
                            <div class="">
                            	
                                <button type="submit" value="Guardar" class="btn btn-primary">Registrar</button>
                                <a class="btn btn-danger" href="index.php">Cancelar</a>
                                
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="">
                                
                               
                                
                            </div>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>



	
	<script type="text/javascript" src="resources/js/Jquery.js"></script>
	<script type="text/javascript" src="resources/js/materialize.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('select').formSelect();
		});
	</script>
</body>
</html>