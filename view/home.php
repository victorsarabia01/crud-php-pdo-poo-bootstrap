<?php
	//include_once 'controller/control.php';
?>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="resources/css/bootstrap.css">
</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h3>Datos Persona</h3>
			</div>
		</div>
		<div class="">
			<a href="?c=nuevo" class="btn btn-block btn-success">Nuevo Registro</a>
		</div>
		
		<br>
		<div class="row">
			<div class="col-md-12 text-center">
				<table class="table">
					<tr class="table-secondary">
						<th>ID</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Edad</th>
						<th>Fecha</th>
						<th>Genero</th>
						<th></th>
						<th></th>
					</tr>
					<?php foreach ($this->mode->listar() as $k) : ?>
					
						<tr>
							<td><?php echo $k->id; ?></td>
							<td><?php echo $k->nombre; ?></td>
							<td><?php echo $k->apellido; ?></td>
							<td><?php echo $k->edad; ?></td>
							<td><?php echo $k->fecha; ?></td>
							<td><?php echo $k->nombre_genero; ?></td>
							<td>
								<a href="?c=nuevo&id=<?php echo $k->id; ?>" class="btn btn-primary">>Editar<</a>
							</td>
							<td>
								<a href="?c=eliminar&id=<?php echo $k->id; ?>" class="btn btn-danger">Eliminar</a>
							</td>

						</tr>

					<?php endforeach; ?>
					
				</table>
				<div class="row">
				<a href="?c=nuevo" class="btn btn-block btn-success">Nuevo Registro</a>
				</div>
				
			</div>
		</div>
	</div>

</body>
</html>