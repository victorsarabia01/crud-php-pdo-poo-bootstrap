-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-08-2022 a las 22:27:01
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbphp18`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dboalumno`
--

CREATE TABLE `dboalumno` (
  `id` int(11) NOT NULL,
  `nombre` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `apellido` varchar(45) COLLATE utf8_spanish_ci NOT NULL,
  `edad` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `id_genero` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `dboalumno`
--

INSERT INTO `dboalumno` (`id`, `nombre`, `apellido`, `edad`, `fecha`, `id_genero`) VALUES
(1, 'victor', 'sarabia', 28, '1994-04-24', 1),
(3, 'aura', 'caldera', 28, '1994-04-24', 2),
(5, 'juana', 'arroyo', 28, '1994-01-26', 2),
(7, 'daniel', 'sanchez', 38, '1988-07-06', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dbogenero`
--

CREATE TABLE `dbogenero` (
  `id_genero` int(11) NOT NULL,
  `nombre_genero` varchar(45) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `dbogenero`
--

INSERT INTO `dbogenero` (`id_genero`, `nombre_genero`) VALUES
(1, 'masculino'),
(2, 'femenino');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `dboalumno`
--
ALTER TABLE `dboalumno`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk1` (`id_genero`);

--
-- Indices de la tabla `dbogenero`
--
ALTER TABLE `dbogenero`
  ADD PRIMARY KEY (`id_genero`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `dboalumno`
--
ALTER TABLE `dboalumno`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `dbogenero`
--
ALTER TABLE `dbogenero`
  MODIFY `id_genero` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `dboalumno`
--
ALTER TABLE `dboalumno`
  ADD CONSTRAINT `fk1` FOREIGN KEY (`id_genero`) REFERENCES `dbogenero` (`id_genero`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
