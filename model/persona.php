<?php 

	class persona{
		public $CNX;
		public $id;
		public $nombre;
		public $apellido;
		public $edad;
		public $fecha;
		public $id_genero;


		public function __construct(){
			try {
				$this->CNX = conexion::conectar();
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function listar(){
			try {
				$query="SELECT a.id,a.nombre,a.apellido,a.edad,a.fecha,g.nombre_genero FROM dboalumno a INNER JOIN dbogenero g on a.id_genero=g.id_genero";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function delete ($id){
			try {
				$query="DELETE FROM dboalumno WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cargarID($id){
			try {
				$query="SELECT * FROM dboalumno WHERE id=?";
				$smt = $this->CNX->prepare($query);
				$smt->execute(array($id));
				return $smt->fetch(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function registrar(persona $data){
			try {
				$query="Insert into dboalumno (nombre,apellido,edad,fecha,id_genero) values (?,?,?,?,?)";
				$this->CNX->prepare($query)->execute(array($data->nombre,$data->apellido,$data->edad,$data->fecha,$data->id_genero));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function actualizarDatos($data){
			try {
				$query="UPDATE dboalumno set nombre=?,apellido=?,edad=?,fecha=?,id_genero=? WHERE id=?";
				$this->CNX->prepare($query)->execute(array($data->nombre,$data->apellido,$data->edad,$data->fecha,$data->id_genero,$data->id));
				
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}
		public function cargarGenero(){
			try {
				$query="SELECT * FROM dbogenero";
				$smt = $this->CNX->prepare($query);
				$smt->execute();
				return $smt->fetchAll(PDO::FETCH_OBJ);
			} catch (Exception $e) {
				die($e->getMessage());
			}
		}

	} 

?>